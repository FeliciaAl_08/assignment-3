﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;

using System.Collections.Generic;

using UnityEngine.UI;
using System.Runtime.InteropServices;

public class PlayerController : MonoBehaviour
{
    public Text labelText;
    public Text scoreText;
    bool notdone = true;

    private int numLine = -1;
    private string filename = "Read.txt";

    void Start()
    {
        labelText.text = "Lines: ";
        scoreText.text = DLLTest.getNum() + "\n Press 'Space' to read lines and go to next line!\nPress 'Backspace' to go to previous line!";
    }

    void Update()
    {
        DLLTest.Init(filename);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            numLine++;
            labelText.text = Marshal.PtrToStringAnsi(DLLTest.getCharacterName(numLine)) + ": ";
            scoreText.text = Marshal.PtrToStringAnsi(DLLTest.getLine(numLine));
        }
        else if (Input.GetKeyDown(KeyCode.Backspace))
        {
            numLine--;
            labelText.text = Marshal.PtrToStringAnsi(DLLTest.getCharacterName(numLine)) + ": ";
            scoreText.text = Marshal.PtrToStringAnsi(DLLTest.getLine(numLine));
        }

    }

}