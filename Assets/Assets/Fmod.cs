﻿using System;
using System.Runtime.InteropServices;

public class Fmod
{
    [DllImport("Fmod_Plugin")]
    public static extern void initialize(int m);

    [DllImport("Fmod_Plugin")]
    public static extern void update();

    [DllImport("Fmod_Plugin")]
    public static extern void createSound(string s, int m);

    [DllImport("Fmod_Plugin")]
    public static extern void playSounds(int m);

    [DllImport("Fmod_Plugin")]
    public static extern void pauseChannel();

    [DllImport("Fmod_Plugin")]
    public static extern void updateChannelPos(float x, float y, float z);

    [DllImport("Fmod_Plugin")]
    public static extern void updateChannelVel(float x, float y, float z);

    [DllImport("Fmod_Plugin")]
    public static extern void updateListenerPos(float x, float y, float z);

    [DllImport("Fmod_Plugin")]
    public static extern void updateListenerVel(float x, float y, float z);

    [DllImport("Fmod_Plugin")]
    public static extern void updateListenerUp(float x, float y, float z);

    [DllImport("Fmod_Plugin")]
    public static extern void updateListenerFor(float x, float y, float z);

}