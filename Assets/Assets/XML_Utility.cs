﻿using UnityEngine.SceneManagement;
using System.IO;
using System;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Generic;

public class XML_Utility
{
    public static string UTFbyteToString(byte[] ch)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        string constString = encoding.GetString(ch);

        return (constString);
    }

    public static byte[] UTFStringtoByte(string XMLString)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        byte[] byteArray = encoding.GetBytes(XMLString);

        return byteArray;
    }

    public static string serializeObject(object serializeObj)
    {
        string StringXML = null;
        MemoryStream memoryStream = new MemoryStream();
        XmlSerializer xs = new XmlSerializer(typeof(List<SerializeData.ListOfItem>));
        XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);

        xs.Serialize(xmlTextWriter, serializeObj);
        memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
        StringXML = UTFbyteToString(memoryStream.ToArray());
        return StringXML;
    }

    public static object deserializeObject(string a_StringXML) // deserialize object
    {
        XmlSerializer xs = new XmlSerializer(typeof(List<SerializeData.ListOfItem>));
        MemoryStream memoryStream = new MemoryStream(UTFStringtoByte(a_StringXML));
        return xs.Deserialize(memoryStream);
    }

    
}