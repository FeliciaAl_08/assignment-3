﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawn : MonoBehaviour
{
    public static void SpawnCoin(Transform coin)
    {
        Vector3 objPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        objPos.z = 0.0f;
        Instantiate(coin, objPos, Quaternion.Euler(new Vector3(0, 0, 0)));

        
    }
}