﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameTimer : MonoBehaviour {
    public float time = 10.0f;
    //public static bool count = false;
    public string DisplayTime;

    public Text Timer;

    string TimerDisplay;
	// Use this for initialization
	void Start ()
    {
        TimerDisplay = time.ToString();
	}
	
	// Update is called once per frame
	public void Update ()
    {
	    if(enabled)
        {
            time -= Time.deltaTime;
            print(time);
            TimerDisplay = time.ToString();
            if(time <=0)
            {
                print("Game Over");
                SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
            }
        }

        if (!enabled) return;
	}

    public void TimerCountdown()
    {

    }

    private void OnGUI()
    {
        GUI.Label(new Rect(10, 40, 100, 20), TimerDisplay);
    }

}
