#pragma once
#include <windows.h>
#include <vector>
#include <stdio.h>
#include <conio.h>
#include <math.h>

#include "include\FMOD\fmod.hpp"
#include "include\FMOD\fmod_errors.h"

static FMOD::System* sysPtr;

class System
{
public:
	System(FMOD::System* _sysPtr);
	~System();

private:
	FMOD_RESULT      result;
	int              key, numdrivers;
	unsigned int     version;
	FMOD_SPEAKERMODE speakermode;
	FMOD_CAPS        caps;
	char             name[256];
};

class Sound3D
{
public:
	// Constructor/Destructor
	Sound3D(FMOD::System* _sysPtr, FMOD::Channel* _channel, int _maxSize);
	~Sound3D();

	// Control
	void load(char* _fileName, int _numSound);
	void play(int _numSound);
	void pause();

	// Position
	void setPosition(FMOD_VECTOR _pos);
	void setPosition(float _x, float _y, float _z);
	FMOD_VECTOR getPosition();

	// Velocity
	void setVelocity(FMOD_VECTOR _vel);
	void setVelocity(float _x, float _y, float _z);
	FMOD_VECTOR getVelocity();

	//FMOD::System	 *system;

private:
	FMOD::Channel*	 channelPtr;
	//FMOD::System*	 system;
	std::vector<FMOD::Sound*> soundPtr;

	FMOD_VECTOR		 pos;
	FMOD_VECTOR		 vel;
	FMOD_RESULT      result;
	int              key, numdrivers;
	unsigned int     version;
	FMOD_SPEAKERMODE speakermode;
	FMOD_CAPS        caps;
	char             name[256];
};