#include "Sound3D.h"
#include <iostream>

void ERRCHECK(FMOD_RESULT result)
{
	if (result != FMOD_OK)
		printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
}

Sound3D::Sound3D(FMOD::System* _sysPtr, FMOD::Channel* _channel, int _maxSize)
{
	sysPtr = _sysPtr;
	//system = _system;
	channelPtr = _channel;

	vel = { 0.0f, 0.0f, 0.0f };
	pos = { 0.0f, 0.0f, 0.0f };

	soundPtr.reserve(_maxSize);
}

Sound3D::~Sound3D()
{
	for (int i = 0; i < soundPtr.size(); i++)
	{
		result = soundPtr[i]->release();
		ERRCHECK(result);
	}
}

void Sound3D::load(char* _fileName, int _numSound)
{
	result = sysPtr->createSound(_fileName, FMOD_3D, 0, &soundPtr[_numSound]);
	ERRCHECK(result);
	//result = soundPtr[_numSound]->set3DMinMaxDistance(0.5f, 5000.0f);
	//ERRCHECK(result);
	result = soundPtr[_numSound]->setMode(FMOD_LOOP_OFF);
	ERRCHECK(result);
	std::cout << _fileName << std::endl;
}

void Sound3D::play(int _numSound)
{
	result = channelPtr->setPaused(false);
	ERRCHECK(result);
	result = sysPtr->playSound(FMOD_CHANNEL_FREE, soundPtr[_numSound], true, &channelPtr);
	ERRCHECK(result);
	//result = channelPtr->set3DAttributes(&pos, &vel);
	//ERRCHECK(result);
}

void Sound3D::pause()
{
	result = channelPtr->setPaused(true);
	ERRCHECK(result);
}

void Sound3D::setPosition(FMOD_VECTOR _pos)
{
	pos = _pos;
}

void Sound3D::setPosition(float _x, float _y, float _z)
{
	pos = { _x, _y, _z };
}

FMOD_VECTOR Sound3D::getPosition()
{
	return pos;
}

void Sound3D::setVelocity(FMOD_VECTOR _vel)
{
	pos = _vel;
}

void Sound3D::setVelocity(float _x, float _y, float _z)
{
	vel = { _x, _y, _z };
}

FMOD_VECTOR Sound3D::getVelocity()
{
	return vel;
}


System::System(FMOD::System* _sysPtr)
{
	sysPtr = _sysPtr;

	if (FMOD::System_Create(&sysPtr) != FMOD_OK)
	{
		result = FMOD::System_Create(&sysPtr); 
		ERRCHECK(result);
	}
	else
	{
		int numDrivers = 0;
		sysPtr->getNumDrivers(&numDrivers);
		if (numDrivers == 0)
		{
			result = sysPtr->getNumDrivers(&numDrivers);
			ERRCHECK(result);
		}
		else
		{
			sysPtr->init(100, FMOD_INIT_NORMAL, NULL);
			std::cout << "System Initialized" << std::endl;
			sysPtr->setSpeakerMode(speakermode);
			sysPtr->set3DSettings(1.0f, 1.0f, 1.0f);
		}
	}
	/*
	Create a System object and initialize.
	*/
	//result = FMOD::System_Create(&sysPtr);
	//ERRCHECK(result);
	//
	//result = sysPtr->getVersion(&version);
	//ERRCHECK(result);
	//
	//result = sysPtr->getNumDrivers(&numdrivers);
	//ERRCHECK(result);
	//
	//if (numdrivers == 0)
	//{
	//	result = sysPtr->setOutput(FMOD_OUTPUTTYPE_NOSOUND);
	//	ERRCHECK(result);
	//}
	//else
	//{
	//	//have fmod match the speaker mode of my computer
	//	result = sysPtr->getDriverCaps(0, &caps, 0, &speakermode);
	//	ERRCHECK(result);
	//
	//	result = sysPtr->setSpeakerMode(speakermode);       /* Set the user selected speaker mode. */
	//	ERRCHECK(result);
	//
	//	if (caps & FMOD_CAPS_HARDWARE_EMULATED)             /* The user has the 'Acceleration' slider set to off!  This is really bad for latency!. */
	//	{                                                   /* You might want to warn the user about this. */
	//		result = sysPtr->setDSPBufferSize(1024, 10);
	//		ERRCHECK(result);
	//	}
	//
	//	result = sysPtr->getDriverInfo(0, name, 256, 0);
	//	ERRCHECK(result);
	//
	//	//if (strstr(name, "SigmaTel"))   /* Sigmatel sound devices crackle for some reason if the format is PCM 16bit.  PCM floating point output seems to solve it. */
	//	//{
	//	//	result = system->setSoftwareFormat(48000, FMOD_SOUND_FORMAT_PCMFLOAT, 0, 0, FMOD_DSP_RESAMPLER_LINEAR);
	//	//	ERRCHECK(result);
	//	//}
	//}
	//
	//result = sysPtr->init(100, FMOD_INIT_NORMAL, 0);
	//if (result == FMOD_ERR_OUTPUT_CREATEBUFFER)         /* Ok, the speaker mode selected isn't supported by this soundcard.  Switch it back to stereo... */
	//{
	//	result = sysPtr->setSpeakerMode(FMOD_SPEAKERMODE_STEREO);
	//	ERRCHECK(result);
	//
	//	result = sysPtr->init(100, FMOD_INIT_NORMAL, 0);/* ... and re-init. */
	//	ERRCHECK(result);
	//}
	//
	//
	///*
	//Set the distance units. (meters/feet etc).
	//*/
	//result = sysPtr->set3DSettings(1.0, 1.0f, 1.0f);
	//ERRCHECK(result);

}

System::~System()
{
	result = sysPtr->close();
	ERRCHECK(result);
	result = sysPtr->release();
	ERRCHECK(result);
}