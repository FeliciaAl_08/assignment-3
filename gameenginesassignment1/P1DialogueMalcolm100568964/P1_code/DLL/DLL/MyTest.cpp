#include "MyTest.h"

extern "C"
{
	float TestMultiply(float a, float b)
	{
		return a * b;
	}

	float TestAdd(float a, float b)
	{
		return a + b;
	}

	void Init(const char *filename)
	{
		lines.clear();
		//return filename;
		std::ifstream file;
		file.open(filename, std::ifstream::in);
		std::string str;
		std::string file_contents;
		while (std::getline(file, str))
		{
			lines.push_back(str);
			file_contents += str;
			file_contents.push_back('\n');
		}

	}

	int getNum()
	{
		return lines.size();
	}

	const char* getLine(int ID)
	{
		if (ID >= lines.size() || ID < 0)
			return " ";

		std::string str;
		bool j = true;
		for (int i = 0; i < lines.at(ID).size(); i++)
		{
			if (j && (lines.at(ID).at(i) == '"'))
			{
				i++;
				j = !j;
				str += lines.at(ID).at(i);
			}
			else if (!j && (lines.at(ID).at(i) == '"'))
			{
				char *ptr = new char[str.length() + 1];

				strcpy_s(ptr, str.length() + 1, str.c_str());

				// Return the pointer to the dynamically allocated buffer
				return ptr;
			}
			else if (!j)
				str += lines.at(ID).at(i);
				
		}
		
		char *ptr = new char[str.length() + 1];

		strcpy_s(ptr, str.length()+1, str.c_str());

		// Return the pointer to the dynamically allocated buffer
		return ptr;
	}

	const char* getCharacterName(int ID)
	{
		if (ID >= lines.size() || ID < 0)
			return "";

		std::string str;
		for (int i = 0; i < lines.at(ID).size(); i++)
		{
			if (lines.at(ID).at(i) == ',')
			{
				char *ptr = new char[str.length() + 1];

				strcpy_s(ptr, str.length() + 1, str.c_str());

				// Return the pointer to the dynamically allocated buffer
				return ptr;
			}
			else
				str += lines.at(ID).at(i);

		}
	}

}

MyTest::MyTest()
{
}


MyTest::~MyTest()
{
}
