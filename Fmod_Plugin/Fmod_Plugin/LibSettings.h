#pragma once
#define FMOD_PLUGIN_EXPORTS

#ifdef FMOD_PLUGIN_EXPORTS
#define LIB_API __declspec(dllexport)
#elif FMOD_PLUGIN_IMPORTS
#define LIB_API __declspec(dllimport)
#else
#define LIB_API
#endif
