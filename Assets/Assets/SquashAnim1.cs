﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animation))]
public class SquashAnim : MonoBehaviour {

    public Animation anim;
    public float m_MaxDistance;
    public float m_SquashValueMultiplier;
    public float m_StretchValueMultiplier;
    public float m_AnimSpeed;


    ///public Color lerpColor = Color.white;

    float SquashMidValue, StretchMidValue, SquashValue1, SquashValue2, StretchValueX, StretchValueY, midDistance;

    
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animation>();
        AnimationCurve curveX, curveZ, curveY, curveScaleX, curveScaleY;
        //AnimationCurve curve = AnimationCurve.Linear(0.0F, 1.0F, 2.0F, 0.0F);
        AnimationClip clip = new AnimationClip();
        clip.legacy = true;

        //SquashValue1 = (m_SquashValueMultiplier) / 1.2f;
        SquashValue1 = transform.localScale.x * m_SquashValueMultiplier;
        SquashValue2 = m_SquashValueMultiplier;

        StretchValueY = (m_StretchValueMultiplier) * 1.2f;
        StretchMidValue = m_StretchValueMultiplier * 1.1f;
        Keyframe[] keysY, keysX, keysZ, keysScaleX, keysScaleY;
        keysY = new Keyframe[3];
        keysX = new Keyframe[3];
        keysZ = new Keyframe[3];
        keysScaleX = new Keyframe[6];
        keysScaleY = new Keyframe[8];

        keysY[0] = new Keyframe(0.0f, m_MaxDistance);
        keysY[1] = new Keyframe(1.0f, transform.position.y);
        keysY[2] = new Keyframe(2.0f, m_MaxDistance);

        keysX[0] = new Keyframe(0.0f, transform.position.x);
        keysX[1] = new Keyframe(1.0f, transform.position.x);
        keysX[2] = new Keyframe(2.0f, transform.position.x);

        keysZ[0] = new Keyframe(0.0f, transform.position.z);
        keysZ[1] = new Keyframe(1.0f, transform.position.z);
        keysZ[2] = new Keyframe(2.0f, transform.position.z);

        // squash
        keysScaleX[0] = new Keyframe(0.0f , transform.localScale.x);
        keysScaleX[1] = new Keyframe(0.5f, transform.localScale.x); //0:30
        keysScaleX[2] = new Keyframe(0.7f , transform.localScale.x); // 0:40
        keysScaleX[3] = new Keyframe(1.0f , SquashValue1);
        keysScaleX[4] = new Keyframe(1.2f , transform.localScale.x); //1:10
        keysScaleX[5] = new Keyframe(2.0f , transform.localScale.x);


        //stretch
        keysScaleY[0] = new Keyframe(0.0f , transform.localScale.y);
        keysScaleY[1] = new Keyframe(0.5f , StretchMidValue); //0:30
        keysScaleY[2] = new Keyframe(0.7f, StretchValueY); //0.40
        keysScaleY[3] = new Keyframe(1.0f , transform.localScale.y); //1:00
        keysScaleY[4] = new Keyframe(1.2f, StretchMidValue);
        keysScaleY[5] = new Keyframe(1.5f , StretchValueY);
        keysScaleY[6] = new Keyframe(1.85f, transform.localScale.y);
        keysScaleY[7] = new Keyframe(2.0f , transform.localScale.y);

        curveY = new AnimationCurve(keysY);
        curveX = new AnimationCurve(keysX);
        curveZ = new AnimationCurve(keysZ);
        curveScaleX = new AnimationCurve(keysScaleX);
        curveScaleY = new AnimationCurve(keysScaleY);

        anim.playAutomatically = true;
        clip.SetCurve("", typeof(Transform), "localPosition.y", curveY);
        clip.SetCurve("", typeof(Transform), "localPosition.x", curveX);
        clip.SetCurve("", typeof(Transform), "localPosition.z", curveZ);
        clip.SetCurve("", typeof(Transform), "localScale.x", curveScaleX);
        clip.SetCurve("", typeof(Transform), "localScale.y", curveScaleY);
        
        anim.AddClip(clip, gameObject.transform.name);
        anim.Play(gameObject.transform.name);
        anim[gameObject.transform.name].speed = m_AnimSpeed;
        anim.enabled = true;

        anim[gameObject.transform.name].wrapMode = WrapMode.PingPong;       
    }
	
	// Update is called once per frame
	void Update ()
    {
       //Color lerpColor = Color.Lerp(Color.white, Color.cyan, Mathf.PingPong(Time.time, 1));
    }
}
