﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public float moveSpeed = 3f;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Rotate(transform.forward, Random.Range(transform.rotation.z - 30f, transform.rotation.z + 30f), Space.World);
        transform.position += transform.right * Time.deltaTime * moveSpeed;

        //Boundaries
        if (transform.position.x < -8.5f)
        {
            moveSpeed = -moveSpeed;
            transform.position = new Vector3(-8.5f, transform.position.y, transform.position.z);
        }
        if (transform.position.x > 8.5f)
        {
            moveSpeed = -moveSpeed;
            transform.position = new Vector3(8.5f, transform.position.y, transform.position.z);
        }
        if (transform.position.y < 0.5f)
        {
            moveSpeed = -moveSpeed;
            transform.position = new Vector3(transform.position.x, 0.5f, transform.position.z);
        }
        if (transform.position.y > 8.5f)
        {
            moveSpeed = -moveSpeed;
            transform.position = new Vector3(transform.position.x, 8.5f, transform.position.z);
        }

        //transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
    }
}
