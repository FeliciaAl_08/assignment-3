﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine.UI;
public class Game_ : MonoBehaviour
{
    [DllImport("HelloWorld")]
    public static extern IntPtr HelloWorld();

    [DllImport("HelloWorld")]
    public static extern void Log(string CharName, string ItemName, string Value);

    string FileLoc, fileName;
    public Text Timer;
    public Transform coinPrefab;
    Vector3 Pos;
    Vector3 StartPos;
    List<SerializeData.ListOfItem> GameItem;

    GameObject[] platforms;
    bool Platformtoggle = false;

    bool Cointoggle = false;
    public Transform mainChar;
    bool rClick = true;

    string data = string.Empty;
    public int toolbarInt = 0;
    public string[] toolbarString = new string[]
    {
        "Platform", "Coin"
    };
    Rect windowRect = new Rect(50, 50, 600, 100);
    void Start()
    {
        SaveCurrentScene();
        GameItem = new List<SerializeData.ListOfItem>();
        StartPos = mainChar.transform.position;

        this.GetComponent<GameTimer>().enabled = false;

        print(Application.dataPath);

    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            CoinSpawn.SpawnCoin(coinPrefab);
        }

        
    }
    void OnGUI()
    {
        FileLoc = Application.dataPath + "/";
        fileName = "savedata.xml";
        DoMyWindow();
    }
    void DoMyWindow()
    {

        if (GUI.Button(new Rect(10, 20, 100, 20), "Save"))
        {
            SaveCurrentScene();

            CreateXML();
            print("Got a click");

        }

        if (GUI.Button(new Rect(110, 20, 100, 20), "Load"))
        {
            LoadXML();

            print("click");
        }

        if (GUI.Button(new Rect(210, 20, 100, 20), "Play"))
        {
            platforms = GameObject.FindGameObjectsWithTag("gameobject") as GameObject[];
            foreach(GameObject body in platforms)
            {
                body.GetComponent<DragScript>().enabled = false;
            }
            rClick = false;

            print(rClick);
            mainChar.transform.position = new Vector3(StartPos.x, StartPos.y, StartPos.z);

            this.GetComponent<GameTimer>().enabled = true;

        }

        toolbarInt = GUI.Toolbar(new Rect(500, 20, 250, 30), toolbarInt, toolbarString);
        
    }

    void LoadXML()
    {
        //print("Load"); 
        StreamReader sr = File.OpenText(Application.dataPath + "/" + "SaveData.xml");
        string n_info = sr.ReadToEnd();
        sr.Close();

        if (data.ToString() != "")
        {
            GameItem = (List<SerializeData.ListOfItem>)XML_Utility.deserializeObject(n_info);

            for (int i = 0; i < GameItem.Count; i++)
            {
                Pos = new Vector3(GameItem[i].posx, GameItem[i].posy, GameItem[i].posz);
                platforms[i].transform.position = Pos;
            }

            Debug.Log("Item count: " + GameItem.Count);
        }

    }
    void LoadData()
    {

    }
    void SaveCurrentScene()
    {
        platforms = GameObject.FindGameObjectsWithTag("gameobject") as GameObject[];
        GameItem = new List<SerializeData.ListOfItem>();
        SerializeData.ListOfItem ItemArray;

        foreach(GameObject body in platforms)
        {
            ItemArray = new SerializeData.ListOfItem();
            ItemArray.ID = body.name + "_" + body.GetInstanceID();
            ItemArray.Name = body.name;
            ItemArray.posx = body.transform.position.x;
            ItemArray.posy = body.transform.position.y;
            ItemArray.posz = body.transform.position.z;

            GameItem.Add(ItemArray);
        }
        data = XML_Utility.serializeObject(GameItem);
    }
    void CreateXML()
    {
        StreamWriter writer;
        FileInfo t = new FileInfo(FileLoc + "/" + fileName);

        if(!t.Exists)
        {
            writer = t.CreateText();
        }
        else
        {
            writer = t.CreateText();
        }

        writer.Write(data);
        writer.Close();
        Debug.Log("Create successful");
    }
}