﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Listener : MonoBehaviour
{
    public GameObject m_player;
    Rigidbody m_RB;

    // Use this for initialization
    void Start ()
    {
        Fmod.initialize(2);
        Fmod.createSound("C:/Users/100568964/Documents/Fmodunity/Assets/Sounds/baap.wav", 0);
        Fmod.createSound("C:/Users/100568964/Documents/Fmodunity/Assets/Sounds/ding.wav", 1);

        Fmod.playSounds(1);
        m_RB = m_player.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Fmod.updateListenerPos(transform.position.x, transform.position.y, transform.position.z);
        Fmod.updateListenerVel(m_RB.velocity.x, m_RB.velocity.y, m_RB.velocity.z);
        Fmod.updateListenerUp(transform.up.x, transform.up.y, transform.up.z);
        Fmod.updateListenerFor(transform.forward.x, transform.forward.y, transform.forward.z);
        Fmod.update();
	}
}
