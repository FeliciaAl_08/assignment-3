#include "Wrapper.h"
#include <vector>
#include <iostream>
#include "include\FMOD\fmod.hpp"
#include "include\FMOD\fmod_errors.h"
/*
	Global variables
	We will change to a proper class base structure
	Currently this can only support 1 sound on 1 thread
*/
FMOD::System *fmodManager;
FMOD_RESULT l_result;
FMOD_SPEAKERMODE l_speakerMode;
std::vector<FMOD::Sound*> g_SOUND;//FMOD::Sound *g_SOUND;
FMOD::Channel *g_Channel;

FMOD_VECTOR Listener;
FMOD_VECTOR Up;
FMOD_VECTOR Forward;
FMOD_VECTOR Velocity;

FMOD_VECTOR Channel = { 1.0f, 1.0f, 1.0f };
FMOD_VECTOR ChannelVel = { 0.0f, 0.0f, 0.0f };


void updateListenerPos(float x, float y, float z)
{
	Listener.x = x;
	Listener.y = y;
	Listener.z = z;
}

void updateListenerVel(float x, float y, float z)
{
	Velocity.x = x;
	Velocity.y = y;
	Velocity.z = z;
}

void updateListenerUp(float x, float y, float z)
{
	Up.x = x;
	Up.y = y;
	Up.z = z;
}

void updateListenerFor(float x, float y, float z)
{
	Forward.x = x;
	Forward.y = y;
	Forward.z = z;
}

/*Initialize the fmod system
Every Fmod function returns a result when given to the function FMOD_ErrorString this function returns human readable errors*/
void initialize(int max_size)
{
	if (FMOD::System_Create(&fmodManager) != FMOD_OK)
	{
		l_result = FMOD::System_Create(&fmodManager);
		std::cout << FMOD_ErrorString(l_result) << std::endl;
	}
	else
	{
		int numDrivers = 0;
		fmodManager->getNumDrivers(&numDrivers);
		if (numDrivers == 0)
		{
			l_result = fmodManager->getNumDrivers(&numDrivers);
			std::cout << FMOD_ErrorString(l_result) << std::endl;
		}
		else
		{
			fmodManager->init(100, FMOD_INIT_NORMAL, NULL);
			std::cout << "System Initialized" << std::endl;
			fmodManager->setSpeakerMode(l_speakerMode);
			fmodManager->set3DSettings(1.0f, 1.0f, 1.0f);
			g_SOUND.reserve(max_size);
		}
	}
	
}
/*
Creating sound by sending sound pointer to fmod system
*/
void createSound(char *soundPath, int soundSlot)
{
	l_result = fmodManager->createSound(soundPath, FMOD_3D, 0, &g_SOUND[soundSlot]);
	std::cout << FMOD_ErrorString(l_result);

}
/*
Setting the attributes of the channel and listener to play the sound
*/

void setVolume(float volume)
{
	l_result = g_Channel->setPaused(true);
	std::cout << FMOD_ErrorString(l_result);

	l_result = g_Channel->setVolume(volume);
	std::cout << FMOD_ErrorString(l_result);

	l_result = g_Channel->setPaused(false);
	std::cout << FMOD_ErrorString(l_result);

	fmodManager->update();
}

void playSounds(int soundSlot)
{
	g_SOUND[soundSlot]->setMode(FMOD_LOOP_OFF);
	l_result = fmodManager->playSound(FMOD_CHANNEL_FREE, g_SOUND[soundSlot], false, &g_Channel);
	std::cout << FMOD_ErrorString(l_result);
}

void updateChannelPos(float x, float y, float z)
{
	Channel.x = x;
	Channel.y = y;
	Channel.z = z;
}

void updateChannelVel(float x, float y, float z)
{
	ChannelVel.x = x;
	ChannelVel.y = y;
	ChannelVel.z = z;
}
/*
Updating the channel which is the "thread" that is playing the sound
*/
void update()
{
	g_Channel->setMode(FMOD_3D_LINEARROLLOFF);
	g_Channel->set3DAttributes(&Channel, &ChannelVel);
	fmodManager->set3DSettings(1.0f, 1.0f, 1.0f);
	fmodManager->set3DListenerAttributes(0, &Listener, &Velocity, &Forward, &Up);
	fmodManager->update();
}