﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
    }

    void OnCollisionEnter()
    {
        Fmod.updateChannelPos(transform.position.x, transform.position.y, transform.position.z);
        Fmod.updateChannelVel(0f, 0f, 0f);
        Fmod.playSounds(0);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
