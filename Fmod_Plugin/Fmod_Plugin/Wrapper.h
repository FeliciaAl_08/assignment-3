#pragma once
#include "LibSettings.h"

/*The point of this header is to define entry points for the DLL
>If you look unity side we declare the functions for to look for in the compile DLL
>In this case there is 4 entry points for unity to access*/
#ifdef __cplusplus
extern "C"
{
#endif
	void LIB_API initialize(int max_size);
	void LIB_API createSound(char *soundPath, int soundSlot);
	void LIB_API playSounds(int soundSlot);
	//void LIB_API pauseChannel();
	void LIB_API setVolume(float volume);
	void LIB_API update();
	void LIB_API updateChannelPos(float x, float y, float z);
	void LIB_API updateChannelVel(float x, float y, float z);
	void LIB_API updateListenerPos(float x, float y, float z);
	void LIB_API updateListenerVel(float x, float y, float z);
	void LIB_API updateListenerUp(float x, float y, float z);
	void LIB_API updateListenerFor(float x, float y, float z);
#ifdef __cplusplus
}
#endif